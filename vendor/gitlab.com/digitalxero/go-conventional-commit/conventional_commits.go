package conventional_commit

import (
	"regexp"
	"strings"
)

// nolint:gochecknoglobals
var expectedFormatRegex = regexp.MustCompile(`(?s)^(?:(?P<category>[^\(!:]+)(?:\((?P<scope>[^\)]+)\))?(?P<breaking>!)?: (?P<description>[^\n\r]+))(?:(?:[\n\r]{1,3}(?P<body>.*))?(?:[\n\r]{3,}(?P<footer>.*)?)|(?:[\n\r]+(?P<body>.*))?)`)

// ParseConventionalCommit takes a commits message and parses it into usable blocks
func ParseConventionalCommit(message string) (commit *ConventionalCommit) {
	match := expectedFormatRegex.FindStringSubmatch(message)

	if len(match) == 0 {
		parts := strings.SplitN(message, "\n", 2)
		parts = append(parts, "")
		return &ConventionalCommit{
			Category:    "chore",
			Major:       strings.Contains(parts[1], "BREAKING CHANGE"),
			Description: parts[0],
			Body:        parts[1],
		}
	}

	result := make(map[string]string)
	for i, name := range expectedFormatRegex.SubexpNames() {
		if i != 0 && name != "" {
			if _, ok := result[name]; ok && strings.TrimSpace(match[i]) == "" {
				continue
			}
			result[name] = strings.TrimSpace(match[i])
		}
	}

	for _, category := range MajorCategories {
		if result["category"] == category {
			result["breaking"] = "!"
			break
		}
	}

	commit = &ConventionalCommit{
		Category:    result["category"],
		Scope:       result["scope"],
		Major:       result["breaking"] == "!" || strings.Contains(result["body"], "BREAKING CHANGE"),
		Description: result["description"],
		Body:        result["body"],
		Footer:      result["footer"],
	}

	if commit.Major {
		return commit
	}

	for _, category := range MinorCategories {
		if result["category"] == category {
			commit.Minor = true
			return commit
		}
	}

	for _, category := range PatchCategories {
		if result["category"] == category {
			commit.Patch = true
			return commit
		}
	}

	return commit
}

func ParseConventionalCommits(messages []string) (commits ConventionalCommits) {
	for _, message := range messages {
		commits = append(commits, ParseConventionalCommit(message))
	}

	return
}