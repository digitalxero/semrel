package git

import (
	semver "github.com/hashicorp/go-version"
	"github.com/xanzy/go-gitlab"
	"path"
	"time"

	actions "gitlab.com/digitalxero/simple-actions"
)

type Tag struct {
	Name string
	SHA string
	Version *semver.Version
}
type Tags []*Tag

func(t *Tags) FromGitlab(glTags []*gitlab.Tag, client *gitlab.Client) {
	for _, gt := range glTags {
		tagPrefix := path.Dir(gt.Name)
		if tagPrefix == "." {tagPrefix = ""}
		tagVersion := path.Base(gt.Name)
		version, _ := semver.NewVersion(tagVersion)
		*t = append(*t, &Tag{
			SHA: gt.Commit.ID,
			Name: gt.Name,
			Version: version,
		})
	}
}

type Commit struct {
	SHA string
	Message string
	Time *time.Time
	// File Paths touched by this commit
	Files []string
}
type Commits []*Commit

func(c *Commits) FromGitlab(glCommits []*gitlab.Commit, client *gitlab.Client) {
	for _, gc := range glCommits {
		var files []string
		if diffs, _, err := client.Commits.GetCommitDiff(gc.ProjectID, gc.ID, nil, nil); err == nil {
			for _, diff := range diffs {
				files = append(files, diff.NewPath, diff.OldPath)
			}
		}
		*c = append(*c, &Commit{
			SHA: gc.ID,
			Message: gc.Message,
			Files: files,
			Time: gc.CommittedDate,
		})
	}
}


// Len return the number of messages in the slice
// required to implement sortable interface https://golang.org/pkg/sort/#Interface
func (v Tags) Len() int {
	return len(v)
}

// Less reports whether the element with
// index i should sort before the element with index j.
// required to implement sortable interface https://golang.org/pkg/sort/#Interface
func (v Tags) Less(i, j int) bool {
	var (
		err error
		v1 = v[i].Version
		v2 = v[j].Version
		t1 = v[i].Name
		t2 = v[j].Name
	)

	if v1 == nil {
		if v1, err = semver.NewVersion(t1); err != nil {
			tagPrefix := path.Dir(t1)
			if tagPrefix == "." {tagPrefix = ""}
			tagVersion := path.Base(t1)
			if v1, err = semver.NewSemver(tagVersion); err != nil {
				v1 = &semver.Version{}
			}
		}
	}

	if v2 == nil {
		if v2, err = semver.NewVersion(t2); err != nil {
			tagPrefix := path.Dir(t2)
			if tagPrefix == "." {tagPrefix = ""}
			tagVersion := path.Base(t2)
			if v2, err = semver.NewSemver(tagVersion); err != nil {
				v2 = &semver.Version{}
			}
		}
	}

	return v1.LessThan(v2)
}

// Swap swaps the elements with indexes i and j.
// required to implement sortable interface https://golang.org/pkg/sort/#Interface
func (v Tags) Swap(i, j int) {
	v[i], v[j] = v[j], v[i]
}

type gitActionContext interface {
	actions.ActionContext
	Client() interface{}
	WithClient(interface{}) gitActionContext
	Tags() Tags
	WithTags(Tags) gitActionContext
	Commits() Commits
	WithCommits(Commits) gitActionContext
}