package git

import (
	"fmt"
	"github.com/google/go-github/github"
	semver "github.com/hashicorp/go-version"
	"github.com/xanzy/go-gitlab"
	"path"
	"strings"

	"gitlab.com/digitalxero/simple-actions"
)

type getTagsAction struct {
	*baseGitAction
}

// NewGetTagsAction get all tags for a gitlab project
func NewGetTagsAction(projectID string) actions.Action {
	return &getTagsAction{
		&baseGitAction{projectID, "", ""},
	}
}
// NewGetTagsForSubProjectAction get tags for a subproject when using a mono repo where you tag each subproject as
// <subprojectName>/<subProjectSemver> (eg projA/v1.2.3)
func NewGetTagsForSubProjectAction(projectID, subprojectName string) actions.Action {
	a := NewGetTagsAction(projectID).(*getTagsAction)
	a.subprojectName = subprojectName

	return a
}

// NewGetTagsForBranchAction get tags for a branch, this is only useful if you are tagging prereleases
// eg v1.2.3-<branchName>.<timestamp>
func NewGetTagsForBranchAction(projectID, branch string) actions.Action {
	a := NewGetTagsAction(projectID).(*getTagsAction)
	a.branch = branch

	return a
}

// NewGetTagsForSubprojectOnBranchAction get tags for a branch, this is only useful if you are tagging prereleases and when using a mono repo where you tag each subproject as
// <subprojectName>/<subProjectSemver>-<branchName>.<timestamp> (eg projA/v1.2.3-featureA.20200523222454)
func NewGetTagsForSubprojectOnBranchAction(projectID, subprojectName, branch string) actions.Action {
	a := NewGetTagsAction(projectID).(*getTagsAction)
	a.subprojectName = subprojectName
	a.branch = branch

	return a
}

// Execute docs
func (a *getTagsAction) Execute(ctx actions.ActionContext) (err error) {
	var (
		ok   bool
		gctx gitActionContext
	)

	if gctx, ok = ctx.(gitActionContext); !ok {
		return fmt.Errorf("wrong context type: %T", ctx)
	}

	ctx.Status().Start(fmt.Sprintf("Fetching Project %s tags", a.projectID))
	defer func() {
		ctx.Status().End(err == nil)
	}()
	if ctx.IsDryRun() {
		return nil
	}

	switch client := gctx.Client().(type) {
	case *gitlab.Client:
		err = a.executeGitlab(gctx, client)
	case *github.Client:
		// do github
	default:
		// do local
	}

	if err != nil {
		return err
	}

	if a.subprojectName == "" && a.branch == "" {
		// no filters set, so tags are already filtered
		return nil
	}

	var filteredTags = make(Tags, 0)
	for _, tag := range gctx.Tags() {
		tagPrefix := path.Dir(tag.Name)
		if tagPrefix == "." {tagPrefix = ""}
		if !strings.HasPrefix(tagPrefix, a.subprojectName) {
			continue
		}
		filteredTags = append(filteredTags, tag)
	}

	gctx.WithTags(filteredTags)
	filteredTags = make(Tags, 0)
	if a.branch != "" {
		for _, tag := range gctx.Tags() {
			tagPrefix := path.Dir(tag.Name)
			if tagPrefix == "." {tagPrefix = ""}
			tagVersion := path.Base(tag.Name)
			version, _ := semver.NewVersion(tagVersion)
			if !strings.Contains(version.Prerelease(), a.branch) && !strings.Contains(version.Prerelease(), strings.ReplaceAll(a.branch, ".", "-")) {
				continue
			}
			filteredTags = append(filteredTags, tag)
		}
	}

	if len(filteredTags) == 0 {
		return nil
	}

	gctx.WithTags(filteredTags)

	return nil
}


func (a *getTagsAction) executeGitlab(ctx gitActionContext, client *gitlab.Client) (err error) {
	var (
		glTags []*gitlab.Tag
		tags Tags
	)

	if glTags, _, err = client.Tags.ListTags(a.projectID, &gitlab.ListTagsOptions{ListOptions: gitlab.ListOptions{PerPage: 10}}); err != nil {
		return err
	}

	tags = make(Tags, 0)
	tags.FromGitlab(glTags, client)
	ctx.WithTags(tags)
	return nil
}