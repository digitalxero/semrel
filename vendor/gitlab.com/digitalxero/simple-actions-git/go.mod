module gitlab.com/digitalxero/simple-actions-git

go 1.13

require (
	github.com/AlekSi/pointer v1.1.0
	github.com/google/go-github v17.0.0+incompatible
	github.com/hashicorp/go-retryablehttp v0.6.7 // indirect
	github.com/hashicorp/go-version v1.2.1
	github.com/jdxcode/netrc v0.0.0-20190329161231-b36f1c51d91d
	github.com/xanzy/go-gitlab v0.38.1
	gitlab.com/digitalxero/simple-actions v1.3.1
	golang.org/x/net v0.0.0-20201002202402-0a1ea396d57c // indirect
	golang.org/x/oauth2 v0.0.0-20200902213428-5d25da1a8d43 // indirect
	golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e // indirect
	sigs.k8s.io/kind v0.9.0
)
