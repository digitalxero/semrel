package git

import (
	"fmt"
	"sort"

	actions "gitlab.com/digitalxero/simple-actions"
)

type sortTagsActions struct {
	reverse bool
}


// NewSortTagsAction get all tags for a gitlab project
func NewSortTagsAction(reverse bool) actions.Action {
	return &sortTagsActions{
		reverse: reverse,
	}
}

// Execute docs
func (a *sortTagsActions) Execute(ctx actions.ActionContext) (err error) {
	var (
		ok   bool
		tags Tags
		gctx gitActionContext
	)

	if gctx, ok = ctx.(gitActionContext); !ok {
		return fmt.Errorf("wrong context type: %T", ctx)
	}

	ctx.Status().Start("Sorting tags")
	defer func() {
		ctx.Status().End(err == nil)
	}()
	if ctx.IsDryRun() {
		return nil
	}

	tags = gctx.Tags()

	if len(tags) == 0 {
		// no tags, obviously sorted
		return nil
	}

	if a.reverse {
		sort.Sort(sort.Reverse(tags))
	} else {
		sort.Sort(tags)
	}

	gctx.WithTags(tags)

	return nil
}
