package git

import (
	"fmt"
	"net/http"

	"github.com/jdxcode/netrc"
	"github.com/xanzy/go-gitlab"
	"sigs.k8s.io/kind/pkg/log"

	"gitlab.com/digitalxero/simple-actions/term"
)

var (
	DefaultAPIVersion = "v4"
)

type baseGitAction struct {
	projectID,
	branch,
	subprojectName string
}

// GitlabClient return a gitlab GitlabClient that uses the local .netrc for the specified domain
func GitlabClient(netrcFile, domainName string, httpClient *http.Client) (gitlabClient *gitlab.Client, err error) {
	var (
		netrcData    *netrc.Netrc
		netrcMachine *netrc.Machine
	)

	if netrcData, err = netrc.Parse(netrcFile); err != nil {
		return nil, err
	}
	if netrcMachine = netrcData.Machine(domainName); netrcMachine == nil {
		return nil, fmt.Errorf("missing .netrc entry")
	}

	if httpClient == nil {
		httpClient = &http.Client{}
	}

	if gitlabClient, err = gitlab.NewClient(netrcMachine.Get("password"),
		gitlab.WithBaseURL(fmt.Sprintf("https://%s/api/%s", domainName, DefaultAPIVersion)),
		gitlab.WithHTTPClient(httpClient),
	); err != nil {
		return nil, err
	}

	return gitlabClient, nil
}


type defaultGitlabActionContex struct {
	logger log.Logger
	status *term.Status
	dryRun bool
	tags   Tags
	commits Commits
	client interface{}
}

// NewDefaultActionContext generate a default action context with a nil return for Data()
func NewDefaultActionContext(logger log.Logger, dryRun bool) gitActionContext {
	return &defaultGitlabActionContex{
		logger: logger,
		status: term.StatusForLogger(logger),
		dryRun: dryRun,
	}
}

// Logger returns the logger
func (ac *defaultGitlabActionContex) Logger() log.Logger {
	return ac.logger
}

// Status returns the term.Status
func (ac *defaultGitlabActionContex) Status() *term.Status {
	return ac.status
}

// IsDryRun defines if this should be a dry run
func (ac *defaultGitlabActionContex) IsDryRun() bool {
	return ac.dryRun
}

// Data returns nil
func (ac *defaultGitlabActionContex) Data() interface{} {
	return nil
}

// Client returns client
func (ac *defaultGitlabActionContex) Client() interface{} {
	return ac.client
}

// WithClient sets client
func (ac *defaultGitlabActionContex) WithClient(client interface{}) gitActionContext {
	ac.client = client
	return ac
}

// Tags returns the current tags in this context
func (ac *defaultGitlabActionContex) Tags() Tags {
	return ac.tags
}

// WithTags sets the current tags in this context
func (ac *defaultGitlabActionContex) WithTags(tags Tags) gitActionContext {
	ac.tags = tags
	return ac
}

// Tags returns nil
func (ac *defaultGitlabActionContex) Commits() Commits {
	return ac.commits
}

// Tags returns nil
func (ac *defaultGitlabActionContex) WithCommits(commits Commits) gitActionContext {
	ac.commits = commits
	return ac
}