module gitlab.com/digitalxero/simple-actions

go 1.13

require (
	github.com/drone/envsubst v1.0.2
	github.com/mattn/go-isatty v0.0.12
	golang.org/x/sys v0.0.0-20200523222454-059865788121 // indirect
	sigs.k8s.io/kind v0.8.1
)
