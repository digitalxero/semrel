// Package actions contains the high level workflow actions
package actions

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	"github.com/drone/envsubst"
	"sigs.k8s.io/kind/pkg/log"

	"gitlab.com/digitalxero/simple-actions/term"
)

type defaultActionContext struct {
	logger log.Logger
	status *term.Status
	dryRun bool
}

// NewDefaultActionContext generate a default action context with a nil return for Data()
func NewDefaultActionContext(logger log.Logger, dryRun bool) ActionContext {
	return &defaultActionContext{
		logger: logger,
		status: term.StatusForLogger(logger),
		dryRun: dryRun,
	}
}

// Logger returns the logger
func (ac *defaultActionContext) Logger() log.Logger {
	return ac.logger
}

// Status returns the term.Status
func (ac *defaultActionContext) Status() *term.Status {
	return ac.status
}

// IsDryRun defines if this should be a dry run
func (ac *defaultActionContext) IsDryRun() bool {
	return ac.dryRun
}

// Data returns nil
func (ac *defaultActionContext) Data() interface{} {
	return nil
}

type sleepAction struct {
	duration string
	msg      string
}

// NewSleepAction docs
func NewSleepAction(duration, msg string) Action {
	return &sleepAction{duration: duration, msg: msg}
}

// Execute documentation
func (a *sleepAction) Execute(ctx ActionContext) (err error) {
	var dur time.Duration
	if dur, err = time.ParseDuration(a.duration); err != nil {
		return err
	}

	ctx.Status().Start(fmt.Sprintf("Sleeping for %s %s", a.duration, a.msg))
	defer func() {
		ctx.Status().End(err == nil)
	}()
	if ctx.IsDryRun() {
		return nil
	}

	time.Sleep(dur)

	return nil
}

type printAction struct {
	msg string
}

// NewPrintAction documentation
func NewPrintAction(msg string) Action {
	return &printAction{
		msg: msg,
	}
}

// Execute documentation
func (a *printAction) Execute(ctx ActionContext) error {
	ctx.Status().Start(a.msg)
	ctx.Status().End(true)

	return nil
}

// createDirectoryAction documentation
type createDirectoryAction struct {
	path string
}

// NewCreateDirectoryAction documentation
func NewCreateDirectoryAction(path string) Action {
	path, _ = envsubst.EvalEnv(path)
	path = os.ExpandEnv(path)
	return &createDirectoryAction{
		path: path,
	}
}

// Execute documentation
func (a *createDirectoryAction) Execute(ctx ActionContext) (err error) {
	ctx.Status().Start(fmt.Sprintf("Creating directory %s", a.path))
	defer func() {
		ctx.Status().End(err == nil)
	}()
	if ctx.IsDryRun() {
		return nil
	}

	if err = os.MkdirAll(a.path, os.ModeDir|os.ModePerm); err != nil {
		return err
	}

	return nil
}

// createDirectoryAction documentation
type copyFileAction struct {
	from,
	to,
	path string
}

// NewCopyFileAction documentation
func NewCopyFileAction(from, to string) Action {
	from, _ = envsubst.EvalEnv(from)
	from = os.ExpandEnv(from)
	to, _ = envsubst.EvalEnv(to)
	to = os.ExpandEnv(to)
	return &copyFileAction{
		from: from,
		to:   to,
		path: filepath.Dir(to),
	}
}

// Execute documentation
func (a *copyFileAction) Execute(ctx ActionContext) (err error) {
	var (
		input []byte
	)
	ctx.Status().Start(fmt.Sprintf("Copying file %s => %s", a.from, a.to))
	defer func() {
		ctx.Status().End(err == nil)
	}()
	if ctx.IsDryRun() {
		return nil
	}

	if !FileExists(a.from) {
		err = fmt.Errorf("%s does not exist", a.from)
		return err
	}
	if err = os.MkdirAll(a.path, os.ModeDir|os.ModePerm); err != nil {
		return err
	}

	if input, err = ioutil.ReadFile(a.from); err != nil {
		return err
	}

	err = ioutil.WriteFile(a.to, input, 0644)

	return err
}

// errorAction docs
type errorAction struct {
	err error
	msg string
}

// NewErrorAction docs
func NewErrorAction(err error, msg string) Action {
	return &errorAction{err: err, msg: msg}
}

// Execute documentation
func (a *errorAction) Execute(ctx ActionContext) (err error) {
	ctx.Status().Start(a.msg)
	ctx.Status().End(a.err == nil)

	return a.err
}

// FileExists returns true if the file exists and is not a directory
func FileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// DirExists returns true if the path exists and is a directory
func DirExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return info.IsDir()
}

// FormatDuration format duration string
func FormatDuration(duration time.Duration) string {
	return duration.Round(time.Second).String()
}
