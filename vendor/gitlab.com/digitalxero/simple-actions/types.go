package actions

import (
	"gitlab.com/digitalxero/simple-actions/term"
	"sigs.k8s.io/kind/pkg/log"
)

// ActionContext docs
type ActionContext interface {
	Logger() log.Logger
	Status() *term.Status
	IsDryRun() bool
	Data() interface{}
}

// Action documentation
type Action interface {
	Execute(ctx ActionContext) error
}

// Actions list of action items
type Actions []Action

// Execute executes all action items in the Actions list
func (a Actions) Execute(ctx ActionContext) (err error) {
	for _, action := range a {
		if err = action.Execute(ctx); err != nil {
			return err
		}
	}

	return nil
}
