# simple-actions

This is based somewhat on https://github.com/kubernetes-sigs/kind/tree/93abedaa23f6d4639e0c1b83fc0eb7cacbf357e4/pkg/cluster/internal/create/actions though it is intended to be a little more flexible

* Supports a Dry Run flag for all actions

## Included actions
* docker actions allow you to interact with docker in your actions without needing to call the docker cli directly
* kubectl actions wrap kubectl call in an action interface