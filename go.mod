module gitlab.com/digitalxero/semrel

go 1.13

require (
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/hashicorp/go-version v1.2.1
	github.com/magiconair/properties v1.8.4 // indirect
	github.com/mitchellh/mapstructure v1.3.3 // indirect
	github.com/pelletier/go-toml v1.8.1 // indirect
	github.com/spf13/afero v1.4.1 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/cobra v1.1.0
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/viper v1.7.1
	gitlab.com/digitalxero/go-conventional-commit v1.0.0
	gitlab.com/digitalxero/simple-actions v1.3.1
	gitlab.com/digitalxero/simple-actions-git v0.0.0
	golang.org/x/net v0.0.0-20201016165138-7b1cca2348c0 // indirect
	golang.org/x/sys v0.0.0-20201016160150-f659759dc4ca // indirect
	google.golang.org/appengine v1.6.7 // indirect
	gopkg.in/ini.v1 v1.62.0 // indirect
	sigs.k8s.io/kind v0.9.0
)

replace gitlab.com/digitalxero/simple-actions-git => ../actions/git
