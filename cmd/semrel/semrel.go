package main

import (
	"os"

	"gitlab.com/digitalxero/semrel/pkg/cmd"
)

const gitlabProject = "19057034"

var (
	pkgName = "semrel"
	version = "v0.0.0"
	commit  = "local"
)

func main() {
	//binPath, _ = os.Executable()
	//binName = fmt.Sprintf("%s-%s", runtime.GOOS, runtime.GOARCH)

	if err := cmd.Setup(pkgName, version, commit, gitlabProject).Execute(); err != nil {
		os.Exit(127)
	}
}
