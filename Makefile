.PHONY: all githooks clean deps test lint out_dir

OUT_DIR ?= build
PACKAGE	?= semrel
GOLANGCI_LINT_VERSION ?= v1.21.0
GOARCH  ?= amd64
GOOS    ?= linux
GOFLAGS?=-mod=vendor
ifeq ($(GOOS),windows)
    BIN_EXTENSION=.exe
endif
SEMVER  ?= 0.0.0-$(shell whoami)


all: deps test lint build

init: githooks
	curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b $(shell go env GOPATH)/bin $(GOLANGCI_LINT_VERSION)

githooks:
	git config core.hooksPath .githooks

out_dir:
	mkdir -p $(OUT_DIR)

test: out_dir
	go test -race ./... -tags 'release netgo osusergo' -v -coverprofile=$(OUT_DIR)/coverage.out
	go tool cover -func=$(OUT_DIR)/coverage.out

build: out_dir
	CGO_ENABLED=0 GOOS=$(GOOS) GOARCH=$(GOARCH) go build \
		-v -tags 'release netgo osusergo'  \
		-ldflags '$(linker_flags) -s -w -extldflags "-fno-PIC -static" -X main.pkgName=$(PACKAGE) -X main.version=$(SEMVER) -X main.commit=$(CI_COMMIT_SHORT_SHA) -X main.gitlabProject=$(CI_PROJECT_ID)' \
		-o $(OUT_DIR)/$(PACKAGE)-$(GOOS)-$(GOARCH)$(BIN_EXTENSION) ./cmd/semrel/...

#
# lint go code critical checks
#
# Shortcut for lint for developer
lint: out_dir
	$(if $(CI), make ci_lint, make lint_local)

lint_local:
	golangci-lint run --fix -v

# Running lint with additional checks for sonarscanner
# Used by CI
ci_lint:
	golangci-lint run --out-format checkstyle > $(OUT_DIR)/lint-report.xml

format:
	go fmt ./...

deps:
	go get -u ./...
	go mod tidy
	go mod verify
	go mod vendor

