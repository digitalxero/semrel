package cmd

import (
	"fmt"
	"gitlab.com/digitalxero/semrel/pkg/utils"
	gitActions "gitlab.com/digitalxero/simple-actions-git"
	"sigs.k8s.io/kind/pkg/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func nextVersionCmd(config *viper.Viper, logger log.Logger) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "next-version",
		Short: "determine the next semver for each project",
	}

	cmd.PersistentPreRunE = defaultPreRunFunc(cmd)

	cmd.RunE = func(c *cobra.Command, args []string) (err error) {
		var (
			tag *gitActions.Tag
			commits gitActions.Commits
			releaseBranch bool
		)
		if tag, err = utils.GetCurrentTag("6120", "cloud-mgr", config.Get("git.client"), logger, config.GetBool("dry-run")); err != nil {
			return err
		}
		fmt.Println(tag.Name, " - ", tag.SHA)

		if commits, err = utils.GetCommits("6120", "cloud-mgr", config.Get("git.client"), tag, logger, config.GetBool("dry-run")); err != nil {
			return err
		}

		cCommits := utils.GetConventialCommits(commits)
		verParts := tag.Version.Segments()

		var newVer = fmt.Sprintf("v%d.%d.%d", verParts[0], verParts[1], verParts[2])
		for _, cc := range cCommits {
			fmt.Printf("%+v\n", cc)
			switch {
			case cc.Major:
				newVer = fmt.Sprintf("v%d.%d.%d", verParts[0]+1, 0, 0)
				break
			case cc.Minor:
				newVer = fmt.Sprintf("v%d.%d.%d", verParts[0], verParts[1]+1, 0)
			case cc.Patch && newVer == "":
				newVer = fmt.Sprintf("v%d.%d.%d", verParts[0], verParts[1], verParts[2]+1)
			}
		}

		if !releaseBranch {
			newVer = fmt.Sprintf("%s-%s.%d", newVer, "increase-cpcl-timeout", commits[0].Time.UnixNano())
		}

		fmt.Println(newVer)

		return nil
	}

	return cmd
}
