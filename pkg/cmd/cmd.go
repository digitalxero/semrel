package cmd

import (
	"fmt"
	"os"
	"io"
	"path"
	"strings"

	"sigs.k8s.io/kind/pkg/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/digitalxero/simple-actions/term"
)

// Setup creates the root command and adds all sub commands as well as embeds the kind command
func Setup(pkgName, version, commit, gitlabProject string) *cobra.Command {
	config := setupConfig(pkgName, version, commit, gitlabProject)
	logger := newLogger(config)
	return AddSubCommands(rootCmd(config, logger), config, logger)
}

// AddSubCommands adds all subcommands to the root command.
func AddSubCommands(rootCmd *cobra.Command, config *viper.Viper, logger log.Logger) *cobra.Command {
	rootCmd.AddCommand(nextVersionCmd(config, logger))
	//rootCmd.AddCommand(Restart(config, logger))
	//rootCmd.AddCommand(License(config, logger))
	//
	//rootCmd.AddCommand(Agent(logger))
	//rootCmd.AddCommand(tool.SetupTool(config, defaultPreRunFunc))
	return rootCmd
}

func defaultPreRunFunc(cmd *cobra.Command) func(*cobra.Command, []string) error {
	return func(c *cobra.Command, args []string) (err error) {
		if p := cmd.Parent(); p != nil && p.PersistentPreRunE != nil {
			if err = p.PersistentPreRunE(c, args); err != nil {
				return err
			}
		}

		return nil
	}
}

func newLogger(config *viper.Viper) log.Logger {
	verbosity := log.Level(0)
	if config.GetBool("debug") {
		verbosity = log.Level(3)
	}
	var writer io.Writer = os.Stderr
	if term.IsSmartTerminal(writer) {
		writer = term.NewSpinner(writer)
	}
	return term.NewLogger(writer, verbosity)
}

func setupConfig(pkgName, version, commit, gitlabProject string) (config *viper.Viper) {
	pwd, err := os.Getwd()
	if err != nil {
		pwd = "."
	}
	config = viper.New()
	config.SetEnvPrefix(pkgName)
	config.AutomaticEnv()
	config.SetEnvKeyReplacer(strings.NewReplacer(".", "_", "-", ""))
	config.SetDefault("config.type", "yaml")
	config.SetDefault("config.name", pkgName)
	config.SetDefault("config.path", pwd)
	config.SetDefault("app.name", pkgName)
	config.SetDefault("app.version", version)
	config.SetDefault("app.commit", commit)
	config.SetDefault("app.projectID", gitlabProject)
	config.SetConfigType(config.GetString("config.type"))
	config.SetConfigName(config.GetString("config.name"))
	config.AddConfigPath(config.GetString("config.path"))
	config.SetConfigFile(path.Join(config.GetString("config.path"), fmt.Sprintf("%s.%s", config.GetString("config.name"), config.GetString("config.type"))))

	// Set Defaults
	config.SetDefault("debug", false)
	config.SetDefault("netrc", path.Join(os.Getenv("HOME"), ".netrc"))
	config.SetDefault("commits.patch", []string{
		"fix",
		"refactor",
		"perf",
		"docs",
		"style",
		"bug",
		"test",
		"chore",
		"build",
	})
	config.SetDefault("commits.minor", []string{
		"feat",
		"feature",
		"story",
	})
	config.SetDefault("commits.major", []string{
		"breaking",
	})


	config.ReadInConfig()

	return
}