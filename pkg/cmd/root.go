package cmd

import (
	"strings"

	"sigs.k8s.io/kind/pkg/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	gitActions "gitlab.com/digitalxero/simple-actions-git"
)


func rootCmd(config *viper.Viper, logger log.Logger) (cmd *cobra.Command) {
	var (
		debug,
		skipCI,
		dryRun,
		allowPreRelease bool
		cfgFile,
		versionFile,
		gitAPI,
		gitAPIDomain,
		gitWorkflow,
		netrcFile string
		projects,
		branches []string
	)
	cmd = &cobra.Command{
		Use:   config.GetString("app.name"),
		Args:  cobra.NoArgs,
		Short: "Semantic Release CLI",
		Long:  `CLI for managing semantic releases of git projects.`,
	}

	cmd.PersistentPreRunE = func(c *cobra.Command, args []string) (err error) {
		if cfgFile != "" {
			config.SetConfigFile(cfgFile)
		}

		if err = config.ReadInConfig(); err != nil && !strings.Contains(err.Error(), "no such") {
			return err
		}

		if !config.IsSet("git.client") {
			var gitClient interface{}
			switch config.GetString("release.api") {
			case "gitlab":
				gitClient, err = gitActions.GitlabClient(
					config.GetString("netrc"),
					config.GetString("release.api-domain"),
					nil,
				)
			case "github":
				// TODO: implement github
			default:
				// Nada to do here
			}
			if err != nil {
				return err
			}

			config.Set("git.client", gitClient)
		}

		return nil
	}

	cmd.PersistentFlags().BoolVarP(
		&debug,
		"debug",
		"",
		debug,
		`enable debug logging`)
	config.BindPFlag("debug", cmd.PersistentFlags().Lookup("debug"))

	cmd.PersistentFlags().BoolVarP(
		&dryRun,
		"dry-run",
		"",
		dryRun,
		`do a dry run on all actions`)
	config.BindPFlag("dry-run", cmd.PersistentFlags().Lookup("dry-run"))

	cmd.PersistentFlags().BoolVarP(
		&allowPreRelease,
		"allow-pre-release",
		"",
		allowPreRelease,
		`allow releasing pre-release version to the git release api`)
	config.BindPFlag("release.allowPreRelease", cmd.PersistentFlags().Lookup("allow-pre-release"))

	cmd.PersistentFlags().BoolVarP(
		&skipCI,
		"skip-ci",
		"",
		config.GetBool("release.workflow.skipCI"),
		`allow releasing pre-release version to the git release api`)
	config.BindPFlag("release.allowPreRelease", cmd.PersistentFlags().Lookup("allow-pre-release"))

	cmd.PersistentFlags().StringVarP(
		&netrcFile,
		"netrc",
		"",
		config.GetString("netrc"),
		`.netrc file to use for API calls to the git hosting service`)
	config.BindPFlag("netrc", cmd.PersistentFlags().Lookup("netrc"))

	cmd.PersistentFlags().StringVarP(
		&cfgFile,
		"config-file",
		"c",
		"",
		`config file to use`)
	config.BindPFlag("config-file", cmd.PersistentFlags().Lookup("config-file"))

	cmd.PersistentFlags().StringVarP(
		&versionFile,
		"version-file",
		"",
		".next-version",
		`version file to use`)
	config.BindPFlag("version-file", cmd.PersistentFlags().Lookup("version-file"))

	cmd.PersistentFlags().StringVarP(
		&gitAPI,
		"api",
		"",
		"local",
		`which git hosting service api to use`)
	config.BindPFlag("release.api", cmd.PersistentFlags().Lookup("api"))

	cmd.PersistentFlags().StringVarP(
		&gitAPIDomain,
		"api-domain",
		"",
		"",
		`which git hosting service domain to use (eg gitlab.com, github.com)`)
	config.BindPFlag("release.api-domain", cmd.PersistentFlags().Lookup("api-domain"))

	cmd.PersistentFlags().StringVarP(
		&gitWorkflow,
		"git-workflow",
		"",
		config.GetString("release.workflow.type"),
		`which git workflow to use: commit-tag, tag-commit, tag-only.
commit-tag: is the default and is best if you do not use the current git sha in versioning or docker tags 
	and do not have down stream workflows that may be triggered on this git sha
tag-commit: is most useful when you are using the current git sha in versioning or docker tags
	or have down stream workflows that depend on the git sha
tag-only: is best if you are not modifying any files that are checked in to the repo during this process
	eg. you are not using the chglog feature to generate release notes
	`)
	config.BindPFlag("release.workflow.type", cmd.PersistentFlags().Lookup("git-workflow"))

	cmd.PersistentFlags().StringSliceVarP(
		&projects,
		"project",
		"p",
		projects,
		`project directories to use when using a mono repo`)
	config.BindPFlag("projects", cmd.PersistentFlags().Lookup("projects"))

	cmd.PersistentFlags().StringSliceVarP(
		&branches,
		"release-branch",
		"b",
		branches,
		`branches allowed to release from`)
	config.BindPFlag("release.branches", cmd.PersistentFlags().Lookup("release-branch"))



	return cmd
}