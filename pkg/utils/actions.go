package utils

import (
	actions "gitlab.com/digitalxero/simple-actions"
	"sigs.k8s.io/kind/pkg/log"
	semver "github.com/hashicorp/go-version"

	gitActions "gitlab.com/digitalxero/simple-actions-git"
	cc "gitlab.com/digitalxero/go-conventional-commit"
)

func GetCurrentTag(projectID, subProject string, gitClient interface{}, logger log.Logger, dryRun bool) (tag *gitActions.Tag, err error) {
	actionCTX := gitActions.NewDefaultActionContext(logger, dryRun)
	actionCTX.WithClient(gitClient)
	a := &actions.Actions{
		gitActions.NewGetTagsForSubprojectOnBranchAction(projectID, subProject, "increase-cpcl-timeout"),
		gitActions.NewSortTagsAction(true),
	}

	if err = a.Execute(actionCTX); err != nil {
		return nil, err
	}

	tags := actionCTX.Tags()
	if len(tags) == 0 {
		v, _ := semver.NewVersion("v0.0.0")
		return &gitActions.Tag{
			Name: "v0.0.0",
			SHA: "0000000000000000000000000000000000000000",
			Version: v,
		}, nil
	}

	return tags[0], nil
}

func GetCommits(projectID, subProject string, gitClient interface{}, tag *gitActions.Tag, logger log.Logger, dryRun bool) (commits gitActions.Commits, err error) {
	actionCTX := gitActions.NewDefaultActionContext(logger, dryRun)
	actionCTX.WithClient(gitClient)
	a := &actions.Actions{
		gitActions.NewGetCommitsBetweenForSubprojectOnBranch(projectID, subProject, "increase-cpcl-timeout", "", tag.SHA),
	}

	if err = a.Execute(actionCTX); err != nil {
		return nil, err
	}

	commits = actionCTX.Commits()

	return commits, nil
}

func GetConventialCommits(commits gitActions.Commits)  cc.ConventionalCommits {
	var msgs []string
	for _, commit := range commits {
		msgs = append(msgs, commit.Message)
	}

	return cc.ParseConventionalCommits(msgs)
}